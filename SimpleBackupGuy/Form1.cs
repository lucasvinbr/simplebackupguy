﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SimpleBackupGuy
{
    public partial class BackupGuyWindow : Form
    {
        public BackupGuyWindow(string[] cmdLineArgs)
        {
            
            //if a file was dragged over the program, backup the file(s), then close
            if (cmdLineArgs.Length > 1)
            {
                for(int i = 1; i < cmdLineArgs.Length; i++)
                {
                    DoTheBackup(cmdLineArgs[i]);
                }
                Application.Exit();
            }
            else
            {
                InitializeComponent();
                numericUpDown1.Value = Properties.Settings.Default.numBackups;
            }
        }

        private void Form1_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effect = DragDropEffects.Copy;
            }
            else {
                e.Effect = DragDropEffects.None;
            }
        }

        public void DoTheBackup(string filePath)
        {
            // Code to read the contents of the text file
            if (File.Exists(filePath))
            {
                DateTime now = DateTime.Now;
                string nowTimeStamp = string.Concat(now.Year.ToString(), "_", now.Month.ToString(), "_",
                    now.Day.ToString(), "_", now.Hour.ToString(), "_", now.Minute.ToString(), "_", now.Second.ToString());

                string fileName = Path.GetFileName(filePath),
                    backupDir = string.Concat(filePath, "_backups");
                //make a "backup" directory if one does not already exist
                //if there are too many backups, erase the oldest
                try
                {
                    if (!Directory.Exists(backupDir))
                    {
                        Directory.CreateDirectory(backupDir);
                    }

                    //backup!
                    File.Copy(filePath, string.Concat(backupDir, "//", fileName, "_backup_", nowTimeStamp));

                    string finalMsg = "Created new backup file!";

                    //check if there are too many backups around...
                    string[] filesInBackupDir = Directory.GetFiles(backupDir);

                    int backupsToCull = filesInBackupDir.Length - Properties.Settings.Default.numBackups;

                    while (backupsToCull > 0)
                    {
                        string oldestFile = filesInBackupDir[0];
                        //find oldest file and delete it
                        for (int i = 1; i < filesInBackupDir.Length; i++)
                        {
                            if (File.GetLastWriteTime(filesInBackupDir[i]).CompareTo(File.GetLastWriteTime(oldestFile)) < 0)
                            {
                                oldestFile = filesInBackupDir[i];
                            }
                        }

                        finalMsg = string.Concat(finalMsg, "\n Deleted older backup: ", Path.GetFileName(oldestFile));
                        File.Delete(oldestFile);

                        filesInBackupDir = Directory.GetFiles(backupDir);

                        backupsToCull--;
                    }

                    MessageBox.Show(finalMsg);
                }
                catch (Exception exc)
                {
                    MessageBox.Show(string.Concat("An exception occurred! ", exc.Message));
                }
            }

        }

        private void Form1_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                string[] filePaths = (string[])(e.Data.GetData(DataFormats.FileDrop));

                if (filePaths.Length > 1)
                {
                    DialogResult boxResult = MessageBox.Show
                        ("More than one file has been selected, which means a backup will be made for each file. Proceed?",
                        "More than one file selected", MessageBoxButtons.YesNo);
                    if (boxResult == DialogResult.No) return;
                }

                foreach (string fileLoc in filePaths)
                {
                    DoTheBackup(fileLoc);
                }
            }
        }

        private void ValueChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.numBackups = (int) numericUpDown1.Value;
            Properties.Settings.Default.Save();
        }

    }
}
