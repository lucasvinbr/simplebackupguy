﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SimpleBackupGuy
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            BackupGuyWindow backupGuy = new BackupGuyWindow(args);
            if (args.Length > 0)
            {
                for (int i = 0; i < args.Length; i++)
                {
                    backupGuy.DoTheBackup(args[i]);
                }

                Application.Exit();
            }
            else
            {
                Application.Run(backupGuy);
            }
            
            
            
        }
    }
}
